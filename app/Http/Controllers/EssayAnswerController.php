<?php

namespace App\Http\Controllers;

use App\Models\EssayAnswer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EssayAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Validate form data
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|integer|exists:essay_answers,id',
            'essay_id' => 'required|integer|exists:essays,id',
            'answer' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 200, 'errors' => $validator->getMessageBag()->toarray()]);
        }

        try {
            if ($request->id) {
                $data = $request->except('id', '_token');
                $data['user_id'] = auth()->id();
                EssayAnswer::where('id', $request->id)->update($data);
            } else {
                $data = $request->except('_token');
                $data['user_id'] = auth()->id();
                EssayAnswer::create($data);
            }

            return response()->json(['status' => 200, 'msg' => 'The Essay was submitted. Your teacher will review it soon. You will be notified after your essay is reviewed.']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EssayAnswer  $essayAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(EssayAnswer $essayAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EssayAnswer  $essayAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(EssayAnswer $essayAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EssayAnswer  $essayAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EssayAnswer $essayAnswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EssayAnswer  $essayAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(EssayAnswer $essayAnswer)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function answers()
    {
        try {
            if (request()->query('page')) {
                $essayAnswers = auth()->user()->role == 'student' ?
                    auth()->user()->answers()->with('essay', 'user', 'reviews.user')->orderBy('id', 'desc')->paginate(10) :
                    EssayAnswer::with('essay', 'user', 'reviews.user')
                    ->where(function ($q) {
                        if (auth()->user()->role == 'head_teacher') {
                            $q->whereIn('user_id', auth()->user()->students->pluck('id'));
                        } else {
                            $q->whereIn('user_id', User::where('company_id', auth()->user()->company_id)->pluck('id'));
                        }
                    })
                    ->where('is_submitted', 1)
                    ->orderBy('id', 'desc')
                    ->paginate(10);
            } else {
                $essayAnswers = auth()->user()->answers()->with('essay', 'user', 'reviews.user')->get();
            }

            return response()->json(['status' => 200, 'data' => $essayAnswers]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => $e->getMessage()]);
        }
    }
}
